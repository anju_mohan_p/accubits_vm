package com.accubitsinterview.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accubitsinterview.model.Cash;
import com.accubitsinterview.model.Product;
import com.accubitsinterview.repository.CashRepository;
import com.accubitsinterview.repository.ProductRepository;
import com.accubitsinterview.service.CashService;
import com.accubitsinterview.service.ProductService;

@RestController
@RequestMapping("api/vm")
public class VMController {

	
	@Autowired
	ProductService prodService;
	
	@Autowired
	CashService cashService;

	@GetMapping("/products")
	public List<Product> getAllProducts() {
		return prodService.listAllProducts();
		
	}

	@PostMapping("/products")
	public Product addProduct(@RequestBody Product product) {
		return prodService.addProduct(product);
		
	}
	
	@PostMapping("/cash")
	public Cash addCash(@RequestBody Cash cash) {
		return cashService.addCash(cash);
	}
	@DeleteMapping("/cash")
	public Cash withdrawCash(@RequestBody Cash cash) {
		return cashService.withdrawCash(cash);
	}
	
}
