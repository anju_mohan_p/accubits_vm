package com.accubitsinterview.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accubitsinterview.exception.AmountExceedsLimitException;
import com.accubitsinterview.exception.AmountNotSupportedException;
import com.accubitsinterview.model.Cash;
import com.accubitsinterview.repository.CashRepository;
import com.accubitsinterview.service.CashService;

@Service
public class CashServiceImpl implements CashService {

	@Autowired
	CashRepository cashRepository;

	@Override
	public Cash addCash(Cash cash) {
		// TODO Auto-generated method stub
		int coinCount = 0;
		int value = 0;
		int totalCoins = 0;
		try {
			value = cash.getValue();
			totalCoins = cashRepository.findTotalCoins();

			switch (value) {
			case 1:
				coinCount = checkCoinCount(1);
				break;
			case 2:
				coinCount = checkCoinCount(2);
				break;
			case 5:
				coinCount = checkCoinCount(5);
				break;
			case 10:
				coinCount = checkCoinCount(10);
				break;
			default:
				throw new AmountNotSupportedException();
			}
			int addCount = coinCount + cash.getCoinCount();
			if ((totalCoins == 500) || (addCount > 500)) {
				throw new AmountExceedsLimitException();
			}

			else {
				System.out.println("Count = " + (coinCount + addCount));
				cashRepository.addCash(addCount, value);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return cashRepository.findByValue(value);

	}

	private int checkCoinCount(int i) {
		// TODO Auto-generated method stub
		return cashRepository.findCoinCoint(i);
	}

	@Override
	public Cash withdrawCash(Cash cash) {
		// TODO Auto-generated method stub
		cashRepository.delete(cash);
		return null;
	}

}
