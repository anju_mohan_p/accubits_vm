package com.accubitsinterview.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.accubitsinterview.exception.ExceedsLimitException;
import com.accubitsinterview.model.Product;
import com.accubitsinterview.repository.ProductRepository;
import com.accubitsinterview.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService  {

	@Autowired
	ProductRepository productRepository;

	@Override
	public List<Product> listAllProducts() {
		// TODO Auto-generated method stub
		 return productRepository.findAll();
	}

	@Override
	public Product addProduct(Product product) {
		// TODO Auto-generated method stub
//		int totalProd = productRepository.findTotalProducst();
//		if(totalProd+ product.getCount() >totalProd) {
//			throw new ExceedsLimitException("Product count exceeds limit 120","400");
//		}
//		else {
			return productRepository.save(product);
//		}
		
	}
}
