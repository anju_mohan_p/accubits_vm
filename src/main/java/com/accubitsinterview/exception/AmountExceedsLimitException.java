package com.accubitsinterview.exception;

public class AmountExceedsLimitException  extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String errorMessage;
	private String errorcode;
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getErrorcode() {
		return errorcode;
	}
	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}
	public AmountExceedsLimitException(String errorMessage, String errorcode) {
		super();
		this.errorMessage = errorMessage;
		this.errorcode = errorcode;
	}
	public AmountExceedsLimitException() {
		
	}
}
