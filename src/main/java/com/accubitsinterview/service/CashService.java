package com.accubitsinterview.service;

import org.springframework.stereotype.Service;

import com.accubitsinterview.model.Cash;

@Service
public interface CashService {

	Cash addCash(Cash cash);

	Cash withdrawCash(Cash cash);
}
