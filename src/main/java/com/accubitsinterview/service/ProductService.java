package com.accubitsinterview.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.accubitsinterview.model.Product;

@Service
public interface ProductService {

	List<Product> listAllProducts();

	Product addProduct(Product product);

}
