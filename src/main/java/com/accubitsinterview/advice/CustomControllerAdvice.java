package com.accubitsinterview.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.accubitsinterview.exception.AmountExceedsLimitException;
import com.accubitsinterview.exception.AmountNotSupportedException;
import com.accubitsinterview.exception.ExceedsLimitException;

@ControllerAdvice
public class CustomControllerAdvice {

	@ExceptionHandler(value = AmountNotSupportedException.class)
	public ResponseEntity<String> handleEmptyInputException(AmountNotSupportedException amountNotSupportedExc) {
		return new ResponseEntity<String>("Cash added should be 1,2,5 or 10", HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(value = AmountExceedsLimitException.class)
	public ResponseEntity<String> handleAmountExceedsLimitException(
			AmountExceedsLimitException amountExceedsLimitExceptionExc) {
		return new ResponseEntity<String>("Number of coins exceeds limit 500", HttpStatus.FORBIDDEN);

	}
	@ExceptionHandler(value = ExceedsLimitException.class)
	public ResponseEntity<String> handleExceedsLimitException(
			ExceedsLimitException exceedsLimitExceptionExc) {
		return new ResponseEntity<String>("Number of products exceeds limit 120", HttpStatus.FORBIDDEN);

	}
}
