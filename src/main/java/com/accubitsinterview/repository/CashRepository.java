package com.accubitsinterview.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.accubitsinterview.model.Cash;

@Repository
public interface CashRepository extends JpaRepository<Cash, Long> {

	@Query(value = "Select c.coinCount from Cash c where c.value = ?1")
	int findCoinCoint(int v);

	@Transactional
	@Modifying
	@Query(value = "Update Cash set coinCount = ?1 where value = ?2")
	void addCash(int coinCount, int value);

	@Query(value = "Select c from Cash c where c.value = ?1")
	Cash findByValue(int value);

	@Query(value= "Select SUM(coinCount) from Cash")
	int findTotalCoins();

}
