package com.accubitsinterview.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.accubitsinterview.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{

//	@Query(value= "Select SUM(count) from Product")
//	int findTotalProducst();

}
