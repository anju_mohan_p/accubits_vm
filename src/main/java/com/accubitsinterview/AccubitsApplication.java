package com.accubitsinterview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccubitsApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(AccubitsApplication.class, args);
	}

	
}
